package com.my.webservices.repo;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.my.webservices.domain.EmployeeEntity;
import com.my.webservices.services.EmployeeService;

@Component
public class EmployeeDataOnDemand {

	private Random random = new SecureRandom();
	
	private List<EmployeeEntity> data;
	
	@Autowired
	EmployeeService employeeService;
	

	public EmployeeEntity getRandomEmployee(){
		
		init();
		EmployeeEntity employeeEntity = data.get(random.nextInt(data.size()));
		return employeeEntity;
		
	}
	
	public void init() {
        int from = 0;
        int to = 10;
        data = employeeService.findEmloyeeEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'Employee' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
      
    }

	
}