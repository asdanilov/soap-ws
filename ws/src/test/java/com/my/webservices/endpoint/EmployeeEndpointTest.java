package com.my.webservices.endpoint;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.my.webservices.domain.EmployeeEntity;
import com.my.webservices.employeeservice.GetEmployeeByIdRequest;
import com.my.webservices.employeeservice.GetEmployeeByIdResponse;
import com.my.webservices.repo.EmployeeDataOnDemand;
import com.my.webservices.services.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class EmployeeEndpointTest {

	@Autowired
	EmployeeDataOnDemand dod;

	private EmployeeService employeeServiceMock;

	private EmployeeEndpoint employeeEndpoint;

	@Before
	public void setUp() throws Exception {
		employeeServiceMock = EasyMock.createMock(EmployeeService.class);
		employeeEndpoint = new EmployeeEndpoint(employeeServiceMock);
	}

	@Test
	public void testGetEmployeeById() {

		EmployeeEntity employeeEntity = dod.getRandomEmployee();

		EasyMock.expect(
				employeeServiceMock.findOne(EasyMock.eq(employeeEntity.getId())))
				.andReturn(employeeEntity);
		EasyMock.replay(employeeServiceMock);

		GetEmployeeByIdRequest request = new GetEmployeeByIdRequest();
		request.setId(employeeEntity.getId());
		GetEmployeeByIdResponse responce = employeeEndpoint
				.getEmployeeById(request);
		Assert.assertNotNull("Responce is NULL", responce);

		EasyMock.verify(employeeServiceMock);
	}

	@Test
	public void testGetEmployeeByDepartment() {

	}

}
