package com.my.webservices.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.webservices.domain.EmployeeEntity;
import com.my.webservices.repository.EmployeeRepository;
import com.my.webservices.services.EmployeeService;

@Service
@Transactional(readOnly=true)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;


	public List<EmployeeEntity> findEmloyeeEntries(int min, int max) {
		return employeeRepository.findAll(new PageRequest(min / max, max)).getContent();
	}
	
	public List<EmployeeEntity> findByDepartment(String department) {
		return employeeRepository.findByDepartment(department);
	}

	public EmployeeEntity findOne(Long id) {
		return employeeRepository.findOne(id);
	}
}
