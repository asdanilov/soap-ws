package com.my.webservices.endpoint;

import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.my.webservices.domain.EmployeeEntity;
import com.my.webservices.employeeservice.GetEmployeeByDepartmentRequest;
import com.my.webservices.employeeservice.GetEmployeeByDepartmentResponse;
import com.my.webservices.employeeservice.GetEmployeeByIdRequest;
import com.my.webservices.employeeservice.GetEmployeeByIdResponse;
import com.my.webservices.services.EmployeeService;
import com.my.webservices.utils.SchemaConversionUtils;

/**
 * Endpoint class
 * 
 * @author Alex
 *
 */

@Endpoint
public class EmployeeEndpoint {

	@Autowired
	private EmployeeService employeeService;

	public EmployeeEndpoint() {
	}
	
	
	public EmployeeEndpoint(EmployeeService employeeService) {
		super();
		this.employeeService = employeeService;
	}


	private static final String NAMESPACE_URI = "http://com/my/webservices/employeeservice";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEmployeeByIdRequest")
	@ResponsePayload
	public GetEmployeeByIdResponse getEmployeeById(
			@RequestPayload GetEmployeeByIdRequest request) {
		/*
		 * EmployeeEntity employee = new EmployeeEntity();
		 * employee.setFirstName("Alex"); employee.setLastName("Dan");
		 * employee.setSuName("Serg"); employee.setDepartment("DBO");
		 * employee.setEmail("as@as.com"); employee.setBirthDate(new Date());
		 * employeeService.save(employee);
		 */

		EmployeeEntity employeeEntity = employeeService.findOne(request
				.getId());

		Assert.notNull(employeeEntity, "NULL");
		
		GetEmployeeByIdResponse response = new GetEmployeeByIdResponse();

		try {
			response.setEmployee(SchemaConversionUtils
					.toSchemaType(employeeEntity));
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getEmployeeByDepartmentRequest")
	@ResponsePayload
	public GetEmployeeByDepartmentResponse getEmployeeByDepartment(
			@RequestPayload GetEmployeeByDepartmentRequest request)
			throws NullResult {
		/*
		 * EmployeeEntity employee = new EmployeeEntity();
		 * employee.setFirstName("Alex"); employee.setLastName("Dan");
		 * employee.setSuName("Serg"); employee.setDepartment("DBO");
		 * employee.setEmail("as@as.com"); employee.setBirthDate(new Date());
		 * employeeService.save(employee);
		 */

		List<EmployeeEntity> listEmployeeEntities = employeeService
				.findByDepartment(request.getDepartment());

		Assert.notEmpty(listEmployeeEntities, "NULL");

		GetEmployeeByDepartmentResponse response = new GetEmployeeByDepartmentResponse();

		try {
			
			for(EmployeeEntity employeeEntity : listEmployeeEntities){
				response.getEmployees().add(SchemaConversionUtils.toSchemaType(employeeEntity));	
			}
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return response;
	}

}
