package com.my.webservices.endpoint;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.SERVER)
public class NullResult extends Exception {

	public NullResult() {
		super("no value");
	}
	
}
