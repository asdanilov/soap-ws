package com.my.webservices.utils;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.my.webservices.domain.EmployeeEntity;
import com.my.webservices.employeeservice.Employee;

/**
 * Class to convert Entity to Schema
 * 
 * @author Alex
 *
 */
public class SchemaConversionUtils {

    public static Employee toSchemaType(EmployeeEntity employeeEntity)
            throws DatatypeConfigurationException {
        
    	Employee schemaEmployee = new Employee();
       
        schemaEmployee.setFirstName(employeeEntity.getFirstName());
        schemaEmployee.setLastName(employeeEntity.getLastName());
        schemaEmployee.setSuName(employeeEntity.getSuName());
        schemaEmployee.setPhone(employeeEntity.getPhone());
        schemaEmployee.setEmail(employeeEntity.getEmail());
        schemaEmployee.setDepartment(employeeEntity.getDepartment());
        schemaEmployee.setBirthDate(toXMLGregorianCalendar(employeeEntity.getBirthDate()));
        
        return schemaEmployee;
    }
	
    public static XMLGregorianCalendar toXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {

    	GregorianCalendar c = new GregorianCalendar();
    	
		c.setTime(date);
    	
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    }
}
