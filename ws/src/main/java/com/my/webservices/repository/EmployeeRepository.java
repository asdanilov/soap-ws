package com.my.webservices.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.my.webservices.domain.EmployeeEntity;

/**
 * Employees repo operations
 * 
 * @author Alex
 *
 */
public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long>, JpaSpecificationExecutor<EmployeeEntity> {

	List<EmployeeEntity> findByDepartment(String department);

}
